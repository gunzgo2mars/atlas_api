const express = require('express')
const route = express.Router()
const mongodb = require('mongodb').MongoClient
const ObjectId = require('mongodb').ObjectId


//jPk5rGuGGbi7Io2t
let mongoAcKey = 'mongodb+srv://gunzgo2mars:67756e7a737061636573746174696f6e@cluster0-awosf.mongodb.net/test?retryWrites=true'

route.get('/' , (req , res) => {

    res.status(200).json({ message : 'Atlas : Data api v1' , error : false })
    
})


route.post('/createNewUser' , (req , res , next) => {

    mongodb.connect(mongoAcKey , { useNewUrlParser : true } , (err , db) => {
        
        if (err) res.json({ message : err , error : true })

        const dbo = db.db('atlas_db')
        
        const postRequest = {

            astronautId : req.body.astronautId,
            astronautPassword : req.body.astronautPassword

        }
        
        dbo.collection('astronaut_user').insertOne(postRequest , (err , result) => {

            if (err) {
                res.json({ message : err , error : true })
            }

            res.status(200).json({ message : 'astronaut data has been created.' , error : false })
            db.close()
        })

    })

})


route.post('/userRequestLogin' , (req , res , next) => {

    mongodb.connect(mongoAcKey , { useNewUrlParser : true } , (err , db) => {

        if(err) res.json({ message : err , error : true })

        const dbo = db.db('atlas_db')
        
        const queryRequest = { astronautId : req.body.astronautId , astronautPassword : req.body.astronautPassword }

        dbo.collection('astronaut_user').findOne(queryRequest , (err , result) => {

            if(err) {
                res.json({ message : err , error : true })
            }

            if(result == null) {
                res.status(200).json({ message : 'not found' , error : true })

            } else {
                res.status(200).json({ message : 'found' , error : false })
            }
            db.close()

        })
    
    })

})

route.post('/requestAddNewFriend' , (req ,res , next) => {

    mongodb.connect(mongoAcKey , { useNewUrlParser : true } , (err , db) => {
        
        if (err) res.json({ message : err , error : true })

        const dbo = db.db('atlas_db')
        
        const postRequest = {

            astronautId : req.body.astronautId,
            friendId : req.body.friendId

        }
        
        dbo.collection('friend_ref').insertOne(postRequest , (err , result) => {

            if (err) {
                res.json({ message : err , error : true })
            }

            res.status(200).json({ message : 'friend data has been created.' , error : false })
            db.close()
        })

    })

})

route.post('/getFriendById' , (req , res , next) => {


    mongodb.connect(mongoAcKey , { useNewUrlParser : true } , (err , db) => {
        
        if (err) res.json({ message : err , error : true })

        const dbo = db.db('atlas_db')
        
        dbo.collection('friend_ref').find({astronautId : req.body.astronautId}).toArray((err , result) => {

            if (err) {
                res.json({ message : err , error : true })
            }

            res.status(200).json(result)
            db.close()
            
        })

    })


})

route.post('/deleteFriendById' , (req , res , next) => {

    mongodb.connect(mongoAcKey , { useNewUrlParser : true } , (err , db) => {
        
        if (err) res.json({ message : err , error : true })

        const dbo = db.db('atlas_db')
        
        dbo.collection('friend_ref').deleteOne({ _id : ObjectId(req.body._id) })
            .then(() => {
                res.json({ message : 'Data has been deleted.' , error : false })
            })
            .catch((err) => {
                res.json({ message : err , error : true })
            })

    })

})


route.get('/' , (req , res) => {

    mongodb.connect(mongoAcKey , { useNewUrlParser : true } , (err , db) => {


        if(err) res.json({ message : err , error : true })

        const dbo = db.db('atlas_db')


    })

})



module.exports = route