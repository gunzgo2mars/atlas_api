// Default setup.
const express = require('express')
const helmet = require('helmet')
const cors = require('cors')
const bodyParser = require('body-parser')
// Router
const dataapi = require('./api/v1/dataApi')

// App Use Express.
const app = express()

// PORT
const port = process.env.PORT || 3000

// Use Helmet , Cors and Body-Parser
app.use(cors())
app.use(helmet())
app.use(bodyParser.json())

// App Use Router.

app.get('/' , (req , res) => {

    res.status(200).json({ message : 'Welcome to atlas api (for spu exam)' , error : false })

})

app.use('/api/v1/data' , dataapi)

app.listen(port , () => {

    console.log('App listen to port -> ' + port)

})